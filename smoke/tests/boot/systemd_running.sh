#!/bin/bash

sleep 60

systemctl list-units --state=failed
systemctl list-units --state=failed --full --plain --no-legend | cut -d" " -f1 | xargs -I{} systemctl status {} --no-pager
systemctl status rngd.service --no-pager
dmesg

if ! systemctl is-system-running --wait ; then
  exit 1
fi

