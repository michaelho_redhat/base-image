#!/bin/bash

set -euo pipefail

cat /etc/redhat-release
echo "${DISTRO_NAME}"
cat /etc/redhat-release | grep "${DISTRO_NAME}"
